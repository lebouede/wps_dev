# Objectives
This file intends to describe a procedure to use SRTM data in WPS for the topography with 30m of horizontal resolution.

# Download the data
Download and extract SRTM data over the desired area. Data can be downloaded from [1].

_Note_: One needs to have/create an EarthData account.

# Run the python script
```
python make_wps_static_tiles.py
```

_Note about endian_: you might be using WRF with big or little endian. One can use the check_endian.py script on static data that are known as functionnal to determine the endian. Depending on the result, one might need to change `">u2"` into `"<u2"`.

# Prepare the static data folder
In your WPS_GEOG folder:
- Create a new folder "topo_srtm_1s"
- Copy the tiles produced by the previous script in this folder.
- Paste the index file in this folder. Edit it with the corresponding latitude/longitude of the lower left corner of the data you have downloaded.

# Edit the GEOGRID.TBL
Add to the GEOGRID.TBL:
```
 name = HGT_M
         priority = 2
         dest_type = continuous
         interp_option = srtm_1s:average_gcell(4.0)+four_pt+average_4pt
         rel_path = srtm_1s:topo_srtm_1s
 ===============================
```

# Edit namelist.wps
Add the interpolator sequence:
```
geog_data_res                       = 'srtm_1s',
```

[1]: https://dwtkns.com/srtm30m/
