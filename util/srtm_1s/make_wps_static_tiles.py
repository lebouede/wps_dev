# Script to try to convert the srtm files into a wrf temporary file
import os
import numpy as np
import gdal

# specify the boundaries on which to operate
Nmin=44
Nmax=46
Emin=4
Emax=6

xc=0
yc=0

# Loop over all the directories
for n in np.arange(Nmin, Nmax+1):
    for e in np.arange(Emin, Emax+1):
        ds = gdal.Open(f"./N{n}E00{e}.hgt")
        # Data has to be flipped to coherent with the WPS convention
        # x increases with E
        # y increases with N
        tile = np.flipud(np.array(ds.GetRasterBand(1).ReadAsArray()).astype(">u2"))
        print(f"Min {tile.min()} and MAX : {tile.max()}")
        print(f"tile.shape={tile.shape}")
        shape = tile.shape
        ycp1 = yc + shape[0]
        xcp1 = xc + shape[1]
        name=f"{xc+1:05d}-{xcp1:05d}.{yc+1:05d}-{ycp1:05d}"
        print(f"Making file {name}")
        tile.tofile(os.path.join("tmp",name), format="%d")
        xc =  xcp1
    # Update coefficients
    yc = yc + shape[0]
    xc = 0



