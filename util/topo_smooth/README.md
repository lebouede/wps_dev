# Objectives
Describe the steps to perform the "optimal" smoothing of the topography.

# Gather the data
Get a netCDF file with a field corresponding to the topography.

_Important_: To use this smoothed topography in WRF, one must apply these steps in between `geogrid.exe` and `metgrid.exe`. Therefore, in this example, one uses a geo_em.d0x.nc file.

# Run the optimization script
```
python run_topo_opt.py geo_em.d0x.nc --dxdy 111.111 --topo_var HGT_M --tgt_angle 42 
```

# Basic check 
Check in the figures produced that the optimized topography is compliant to the constraints specified.

# Replace the topography
```
python replace_topo.py geo_em.d0x.nc output_topo.csv --var HGT_M 
```


_Note_: The Python scripts must be run from an environment with the following packages:
- scipy
- xarray
- cvxpy
- numpy
- matplotlib
- wrf-python