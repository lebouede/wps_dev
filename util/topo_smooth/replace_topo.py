import numpy as np
import os
import netCDF4
# import xarray as xr
import argparse
import matplotlib.pyplot as plt
import wrf
from shutil import copyfile, move
import pandas as pd

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Replace topography in geo_em file')
    parser.add_argument('path', type=str, help='Path to the geo_em file file')
    parser.add_argument('newtopo', type=str, help='Path to the new topography')
    parser.add_argument('--hgt', type=str, help='var name for topography in path', default="HGT_M")
    args = parser.parse_args()
    print(f"Modifying topography for {args.path} with {args.newtopo}")

    backup_end = args.path.split('/')[-1] + "_old"
    orig_path = args.path.split('/')[:-1]
    if not orig_path:
        orig_path = ["."]
    orig_path.append(backup_end)
    backup_name = os.path.join(*orig_path)
    print(f"    -> Copying {args.path} to {backup_name}")
    copyfile(args.path, backup_name)

    # Read the topography
    newtopo = np.genfromtxt(args.newtopo, delimiter=',')
    print(newtopo)
    newtopo = np.expand_dims(newtopo,0) #expand it so that it matches shape

    dset = netCDF4.Dataset(args.path, mode="a")
    #ds = xr.open_dataset(args.path)

    topo_orig = dset[args.hgt][:]

    # chech shape
    if topo_orig.shape != newtopo.shape:
        print("Conflicting shapes:")
        print(f"Original shape is {topo_orig.shape}")
        print(f"New topo shape is {newtopo.shape}")
        raise ValueError("Conflicting shapes")


    print(abs(newtopo - topo_orig).sum())

    # Update topography
    print("    -> Update topography values")
    #new_arr = xr.DataArray(np.expand_dims(newtopo,0),
    #                       coords=ds[args.hgt].coords,
    #                       dims=ds[args.hgt].dims)
    #ds[args.hgt] = new_arr

    dset[args.hgt][:] = newtopo


    # Write back to disk
    print("    -> Writing back to disk")
    dset.close()
    #ds.to_netcdf(args.path, mode='w', format='NETCDF4')
    #ds.to_netcdf("tmp.nc", mode='w', format='NETCDF4')
    #move("tmp.nc", args.path)
