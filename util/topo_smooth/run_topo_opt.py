import numpy as np
import xarray as xr
import cvxpy as cp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import argparse
import scipy as sc


def compute_slopes(field, dx):
    diff_x = field[0:-1] - field[1:]
    diff_y = field[:, 0:-1] - field[:, 1:]
    horiz_slope = np.arctan(diff_x / dx) * 180 / np.pi
    vert_slope = np.arctan(diff_y / dx) * 180 / np.pi
    return horiz_slope, vert_slope


def plot_slopes(hslo, vslo, fig_id, maxslo=45, tol=1):
    fig, axes = plt.subplots(1, 1, figsize=(10, 8))
    im1 = axes.pcolormesh(hslo, vmin=-maxslo - tol, vmax=maxslo + tol)
    axes.set_aspect('equal')
    cb1 = fig.colorbar(im1, ax=axes, extend="both")
    cb1.cmap.set_over('red')
    cb1.cmap.set_under('red')
    axes.set_title("Horizontal slopes [in °]")
    fig.savefig(f"hslope_{fig_id}.png", dpi=150)

    fig, axes = plt.subplots(1, 1, figsize=(10, 8))
    im2 = axes.pcolormesh(vslo, vmin=-maxslo - tol, vmax=maxslo + tol)
    axes.set_aspect('equal')
    cb2 = fig.colorbar(im2, ax=axes, extend="both")
    cb2.cmap.set_over('red')
    cb2.cmap.set_under('red')
    axes.set_title("Vertical slopes [in °]")
    fig.savefig(f"vslope_{fig_id}.png", dpi=150)
    return None


def plot_smoothedtopo(topo_cor, figname):
    fig, axes = plt.subplots(1, 1, figsize=(10, 8))
    im1 = axes.pcolormesh(topo_cor, cmap="terrain")
    axes.set_aspect('equal')
    fig.colorbar(im1, ax=axes)
    axes.set_title("Smoothed topography")
    fig.savefig(figname, dpi=150)
    return None

def plot_topocor_dif(topo_cor, topo, figname):
    topo_diff = topo - topo_cor
    fig, axes = plt.subplots(1, 1, figsize=(10, 8))
    maxdiff = np.max([np.max(topo_diff), np.abs(np.min(topo_diff))])
    im2 = axes.pcolormesh(topo_diff, vmin=-maxdiff, vmax=maxdiff, cmap="RdBu_r")
    axes.set_aspect('equal')
    axes.set_title('Original minus smoothed topography ')
    fig.colorbar(im2, ax=axes)
    fig.savefig(figname, dpi=150)
    return None


def maxangle_2_dist(maxangle, dxdy):
    return np.tan(maxangle * np.pi / 180) * dxdy


def optim_pbm(topo, dz_max):
    # Least squares problem

    # Reshape topo in a vector and save dims for later
    nx, ny = topo.shape
    topo_vec = topo.reshape(nx * ny)

    ## Define A and b
    A = sc.sparse.diags(topo_vec).astype(
        np.float
    )  # A is the original topography but as a diagonal matrix to have x1 \times x2 terms
    b = topo_vec.astype(np.float).squeeze()  # b is the original topography

    ## Define the unknown variable in our problem
    n = b.shape  # n is just the numer of unknown in our problem
    x = cp.Variable(n)
    x.values = np.ones(n)

    ## constraints
    gx = np.repeat(dz_max, (nx)*(ny-1)).reshape( ((nx),(ny-1)) )
    gy = np.repeat(dz_max, (nx-1)*(ny)).reshape( ((nx-1),(ny)) )

    g_diag = np.repeat(dz_max * np.sqrt(2),(nx-1)*(ny-1)) # As it is diagonal terms, distance is larger
    g_diag = g_diag.reshape((ny-1,nx-1)) # half of diagonal terms

    constraints = [cp.abs(cp.diff(cp.reshape(A*x,(ny,nx)),k=1,axis=1))  <= gx,
               cp.abs(cp.diff(cp.reshape(A*x,(ny,nx)),k=1,axis=0))  <= gy,
               cp.abs(cp.reshape(A*x,(ny,nx))[:-1,:-1] - cp.reshape(A*x,(ny,nx))[1:,1:]) <= g_diag,
               cp.abs(cp.reshape(A*x,(ny,nx))[1:,0:-1] - cp.reshape(A*x,(ny,nx))[0:-1,1:]) <= g_diag
              ]
    ## Define and solve the CVXPY problem.
    x.value = np.repeat(1,n)
    cost = cp.sum_squares(A*x - b)
    prob = cp.Problem(cp.Minimize(cost),constraints)
    prob.solve(solver=cp.OSQP, verbose=True, max_iter=100000, eps_abs=1e-3, eps_rel=1e-3)

    topo_cor = (A * x).value.reshape(topo.shape)
    return topo_cor, x, prob


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Run optimization algorithm to smooth the topography')
    parser.add_argument('path',
                        type=str,
                        help='Path to the netCDF file with the topography')
    parser.add_argument(
        '--dxdy',
        dest='dxdy',
        default=333.333,
        help='Horizontal resolution of the topography (default: 333.333)',
        type=float)
    parser.add_argument('--topo_var',
                        dest='topo_var',
                        default="HGT",
                        help='Variable name for topography',
                        type=str)
    parser.add_argument('--tgt_angle',
                        default=45.0,
                        help='Target angle in degrees',
                        type=float)
    parser.add_argument('--scale',
                        default=False,
                        help='Do scaling before optimizing ?',
                        type=bool)

    args = parser.parse_args()

    print(
        f"Running optimization algorithm for variable {args.topo_var} in {args.path}:"
    )
    print(f"-> Target angle is {args.tgt_angle}")
    print(f"-> Domain resolution is {args.dxdy}")
    print(f"---------------------------------------------")

    # Read field and extract topo
    ds = xr.open_dataset(args.path)
    tgt = ds.HGT_M.values.squeeze()

    # Take a subset so far (useful for debug only)
    topo = tgt[:, :]

    if args.scale:
        scale = np.max(topo)
        topo = topo / scale

    # Compute slopes and plot them to check afterward if something happened
    if args.scale:
        hslo, vslo = compute_slopes(topo, args.dxdy / scale)
    else:
        hslo, vslo = compute_slopes(topo, args.dxdy)
    plot_slopes(hslo, vslo, f"SlopesTopoOrig_{args.tgt_angle}_{args.dxdy}.png", maxslo=args.tgt_angle)

    # Get Dz from tgt angle:
    if args.scale:
        Dz_max = maxangle_2_dist(args.tgt_angle, args.dxdy / scale)
    else:
        Dz_max = maxangle_2_dist(args.tgt_angle, args.dxdy)

    # Compute the optimization problem
    topo_cor, x, pbm = optim_pbm(topo, Dz_max)

    # Compute the difference between the topographies
    topo_diff = topo - topo_cor

    # Plot slopes on the new topography
    if args.scale:
        hslo, vslo = compute_slopes(topo_cor, args.dxdy / scale)
    else:
        hslo, vslo = compute_slopes(topo_cor, args.dxdy)

    plot_slopes(hslo, vslo, f"{args.tgt_angle}_{args.dxdy}.png", maxslo=args.tgt_angle)

    # Plot the new topography and differences wrt the original one
    plot_smoothedtopo(topo_cor, f"SmoothedTopo_{args.tgt_angle}_{args.dxdy}.png")
    plot_topocor_dif(topo_cor, topo, f"DiffTopo_{args.tgt_angle}_{args.dxdy}.png")

    # Save output array to a file
    np.savetxt(f"SmoothedTopo_{args.tgt_angle}_{args.dxdy}.csv", topo_cor, delimiter=",")
