# Load pollutants in WPS
This procedure intends to allow to load pollutants in the met_em files.
The objective is to introduce a new field with as many layers as pollutants.
This file will also be updated in each met_em file to allow for varying emissions
in time.

_NOTE : This procedure expect the emission file have a certain structure (csv format,
";" as delimiter, Lambert 93 as coordinates...) It is very unlikely that the
reader would be using a similar format. However one can still modify the python
script and use the rest of the procedure as such._

## Preparation
Note: most of the WRF compilations expect binaries to be in the big endian
convention. However we have encountered situations where it is not the
case. To check what is expected by your exectuables, run the `check_endian.py` 
program on one temporary file written by ungrib.

```
python check_endian.py BINARY_FILE
```

The endianness of the file will correspond to the one that outputs a 3,4 or 5
for the first integer value.

Use f2py to convert the fortran function "intermediate-duda.f90"
```
bash make_f2py.sh big
```
```
bash make_f2py.sh little
```

For imports to work properly, the python module produced by this procedure must
be in the same folder as make_emission_intermediate.py.
## Run the python file
Launch the python script with the following arguments:
- path to the emission inventory (absolute or relative)
- Start date : mm/dd/yyyy
- End date : mm/dd/yyyy"
- Number of hours in between each input
- List of string stating which snap number as 01, 02, 03, ... , all

```
python make_emission_intermediate.py ../bilan_maille/bilan_maille_100m_v69_2015_colonnes.mod 12/20/2016 12/21/2016  02 04 07 08 10 all
```

## Edit namelist.wps
in namelist.wps add `PMS` to the line fg_name before
executing metgrid.exe.
```
sed -i "/.*fg_name*/c\ fg_name  ='FILE_SFC','FILE_PL','PMS'" namelist.wps
```

## Edit METGRID.TBL
In METGRID.TBL add the following lines:
```text
========================================
name=PMS
        z_dim_name=trac
        interp_option=average_gcell(3)+four_pt
        fill_missing=0
        missing_value=-999
========================================
```
Here they will be stacked on a new vertical dimension called `trac`
that needs to be added in WRF in order to be read properly.

The option `avereage_gcell` allow to ensure that exactly the same
mass of pollutants will be included in the met_em files (because
emission are stored in kg/hr/m²). 


## Execute metgrid
```
./metgrid.exe
``` 

