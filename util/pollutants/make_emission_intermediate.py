import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
# Add folder of this file to python path to automatically include the fortran auto generated module
sys.path.append(os.path.dirname(__file__))
from WPSUtils import intermediate
from pyproj import Proj, transform
import argparse
import warnings
import shutil

def TNO_monthly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 12)), index=indxs, columns=np.arange(1,13))
    df.loc['01'] = [1.20, 1.15, 1.05, 1.00, 0.90,
                    0.85, 0.80, 0.87, 0.95, 1.00, 1.08, 1.15]
    df.loc['02'] = [1.70, 1.50, 1.30, 1.00, 0.70,
                    0.40, 0.20, 0.40, 0.70, 1.05, 1.40, 1.65]
    df.loc['03'] = [1.10, 1.08, 1.05, 1.00, 0.95,
                    0.90, 0.93, 0.95, 0.97, 1.00, 1.02, 1.05]
    df.loc['04'] = [1.02, 1.02, 1.02, 1.02, 1.02,
                    1.02, 1.00, 0.84, 1.02, 1.02, 1.02, 0.90]
    df.loc['05'] = [1.20, 1.20, 1.20, 0.80, 0.80,
                    0.80, 0.80, 0.80, 0.80, 1.20, 1.20, 1.20]
    df.loc['06'] = [0.95, 0.96, 1.02, 1.00, 1.01,
                    1.03, 1.03, 1.01, 1.04, 1.03, 1.01, 0.91]
    df.loc['07'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07a'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07b'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07c'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['08'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                    1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['09'] = [1.00, 1.00, 1.00, 1.00, 1.00,
                    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10'] = [0.45, 1.30, 2.35, 1.70, 0.85,
                    0.85, 0.85, 1.00, 1.10, 0.65, 0.45, 0.45]
    return df.multiply(df.shape[1]/df.sum(1),'index')


def TNO_dayly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 7)), index=indxs, columns=np.arange(1,8))
    df.loc['01']    = [1.06, 1.06, 1.06, 1.06, 1.06, 0.85, 0.85]
    df.loc['02']    = [1.08, 1.08, 1.08, 1.08, 1.08, 0.80, 0.80]
    df.loc['03']    = [1.08, 1.08, 1.08, 1.08, 1.08, 0.80, 0.80]
    df.loc['04']    = [1.02, 1.02, 1.02, 1.02, 1.02, 1.02, 1.00]
    df.loc['05']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['06']    = [1.20, 1.20, 1.20, 1.20, 1.20, 0.50, 0.50]
    df.loc['07']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07a']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07b']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07c']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['08']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['09']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10']   = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    return df.multiply(df.shape[1]/df.sum(1),'index')

def TNO_hourly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 24)), index=indxs, columns=np.arange(0, 24))
    df.loc['01']    = [0.79, 0.72, 0.72, 0.71, 0.74, 0.80, 0.92, 1.08, 1.19, 1.22, 1.21, 1.21,
                       1.17, 1.15, 1.14, 1.13, 1.10, 1.07, 1.04, 1.02, 1.02, 1.01, 0.96, 0.88]
    df.loc['02']    = [0.38, 0.36, 0.36, 0.36, 0.37, 0.50, 1.19, 1.53, 1.57, 1.56, 1.35, 1.16,
                       1.07, 1.06, 1.00, 0.98, 0.99, 1.12, 1.41, 1.52, 1.39, 1.35, 1.00, 0.42]
    df.loc['03']    = [0.75, 0.75, 0.78, 0.82, 0.88, 0.95, 1.02, 1.09, 1.16, 1.22, 1.28, 1.30,
                       1.22, 1.24, 1.25, 1.16, 1.08, 1.01, 0.95, 0.90, 0.85, 0.81, 0.78, 0.75]
    df.loc['04']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['05']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['06']    = [0.50, 0.35, 0.10, 0.10, 0.20, 0.50, 0.75, 1.25, 1.40, 1.50, 1.50, 1.50,
                       1.50, 1.50, 1.50, 1.50, 1.50, 1.40, 1.25, 1.10, 1.00, 0.90, 0.80, 0.70]
    df.loc['07']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07a']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07b']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07c']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['08']    = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['09']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10']   = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    return df.multiply(df.shape[1]/df.sum(1),'index')


def make_hourly_emission(total_emiss, date, snap:str):
    """
    Arguments:
        total_emiss {np.array} : yearly emission in tons per year in a grid 
            cell of a square kilometer.
        date {pd.Timestamp} : date at which to compute the deconvolution.
        snap {str} : string stating which snap should be used for deconvolution
    """
    mon_fac = TNO_monthly().loc[snap][date.month] # .month from 1 to 12
    day_fac = TNO_dayly().loc[snap][date.dayofweek + 1] # dayofweek from 0 to 6
    hr_fac = TNO_hourly().loc[snap][date.hour]
    date_str = date.strftime("%m/%d/%Y %H:%M")
    print(f"Sn{snap} at {date_str}: hr_fac : {hr_fac:1.2f} | day_fac = {day_fac:1.2f} | mth_fac= {mon_fac:1.2f}")
    #  emissions / (year -> seconds [/ 31536000]) * (ton -> ug [* 10^12]) / (m2 in emission inventory cell [/ 1e6])
    emis_per_sec = total_emiss / 31.536e6 * 1e12 * hr_fac * day_fac * mon_fac / 1e6
    return emis_per_sec

def lam93_28latlon(x, y):
    """Project x and y from Lambert 93 to LatLon
    
    Arguments:
        x {float} -- 
        y {float} -- 
    
    Returns:
        float -- Longitude
        float -- Latitude
    """
    inProj = Proj(init='epsg:2154')  # 2154 stands for Lambert 93
    outProj = Proj(init='epsg:4326')  # 4326 stands for LatLon
    lon, lat = transform(inProj, outProj, x, y)
    return lon, lat

def read_polls_csv(path):
    csv = pd.read_csv(path, sep=";", header=None)
    return csv


def read_emiss2016_csv(path:str):
    """Read the pollutants emission inventory from 
    Atmo AURA produced in 2020 for 2016.
    
    Arguments:
        path {str} -- Path to the csv file
    
    Returns:
        pandas.Dataframe -- Dataframe containing the whole dataset.
    """
    print(f"-> Reading the emission inventory {path}")
    df = pd.read_csv(path, sep=";", header=0, converters={0:float, 1: float, 2:str, 3:str, 4:str, 5:str, 6:str, 7:float, 8:float})
    # Rename the emission column to something less confusing
    df.rename(columns={"sum":"emiss"}, inplace=True)
    # Strip whitespaces from snap_ids
    df['id_snap3'] = df['id_snap3'].str.strip()
    # Convert t to True and others to False
    df["chauffage"] = df["chauffage"] == "t"
    df["chauffage_bois"] = df["chauffage_bois"] == "t"
    # Set xcentre and ycentre as indexes
    # df.set_index(["xcentre","ycentre"],inplace=True)
    return df

def create_field_from_df(df, pol, snap_id):
    # Get global min and max values of staggering
    global_xmin = df.xcentre.min()
    global_xmax = df.xcentre.max()
    global_ymin = df.ycentre.min()
    global_ymax = df.ycentre.max()
    xval = np.arange(global_xmin,global_xmax+1e3, 1e3)
    yval = np.arange(global_ymin,global_ymax+1e3, 1e3)
    # Create a dataframe with index xloc and yloc and ampty values
    iterables =  [xval, yval]
    ind = pd.MultiIndex.from_product(iterables, names=['xcentre', 'ycentre'])
    dfemp = pd.DataFrame(index=ind)
    # Make emissions
    df_pol = df.loc[df['pol'] == pol]
    emiss_snap = df_pol.loc[df.id_snap3.str.startswith(snap_id)]
    emiss_snap = emiss_snap.groupby(['xcentre', 'ycentre']).sum().emiss
    tmp = dfemp.join(emiss_snap)
    tmp.fillna(0, inplace=True)
    field = np.reshape(tmp.values, (len(xval),len(yval)))
    return field, xval, yval

def make_resp_weigths_forall_snaps(df, pol="pm2.5"):
    """ Read the dataframe as created by read_emiss and create 
    a field aggregated for all snaps.

    """
    print(f"Preparing snaps to make the all_snaps field !")
    # make an empty dictionnary to store results
    res_dict= {}
    # Loop over all the snap sectors
    indxs = ["01", "02", "03", "04", "05", "06",
             "07", "08", "09", "10"]
    for idx in indxs:
        print(f"Processing {idx}")
        field, xval, yval = create_field_from_df(df, pol,  idx)
        res_dict[idx] = field 
    return res_dict

def make_all_snaps_field(allsnaps_dict, date):
    """ Function to create a field with all snaps being aggregated together.
    It takes as input a dict with each snap and its corresponding yearly
    emission field and a date.
    """
    snp_idxs = list(allsnaps_dict.keys())
    all_emiss = np.zeros(allsnaps_dict[snp_idxs[0]].shape)
    for idx in snp_idxs:
        all_emiss = all_emiss + make_hourly_emission(allsnaps_dict[idx], date, idx)
    return all_emiss

def df2emissfield(df, pol:str="pm10",codes:list=["02"],debugflag=False):
    print(f"Select the desired pollutants : {pol}")
    df_pol = df.loc[df['pol'] == pol]
    # Make an empty dict
    res = []
    # Compute nx and ny once and for all
    nx = len(df.xcentre.unique())
    ny = len(df.ycentre.unique())
    for k,code in enumerate(codes):
        print(f"Processing {code}")
        # Select all lines with the desired code pattern
        df_code = df_pol.loc[df["id_snap3"].str.startswith(code)]
        df_code.set_index(["xcentre","ycentre"],inplace=True)
        # Group by on xentre and ycentre and then sum to take
        # care of snaps sublevels: Exemple code "02"
        # xcentre ycentre code emiss
        # 1 1   020100  100
        # 1 1   020200  50
        total_emiss = df_code.groupby(["xcentre","ycentre"]).sum()
        # Create a dataframe with index xloc and yloc and ampty values
        iterables =  [np.arange(df.xcentre.unique().min(),df.xcentre.unique().max()+1e3, 1e3),
                      np.arange(df.ycentre.unique().min(),df.ycentre.unique().max()+1e3, 1e3)]
        ind = pd.MultiIndex.from_product(iterables, names=['xcentre', 'ycentre'])
        dfemp = pd.DataFrame(index=ind)
        # merge the dataframes
        tmp = dfemp.join(total_emiss)
        tmp.fillna(0, inplace=True)
        res.append(np.reshape(tmp.emiss.values, (nx, ny)))
    return res

def plot_emissfield(field, code, poll):
    fig, ax = plt.subplots()
    pcm = ax.pcolormesh(np.swapaxes(field,0,1))
    fig.colorbar(pcm, label=f"Emission of {code} in $t/km^2$")
    ax.set_title(f"Surface emission for {code}")
    fig.savefig(f"{poll}_{code}.png")
    return None

if __name__ == "__main__":
    # Parse arguments fo the python script
    parser = argparse.ArgumentParser(epilog=(
        "Example: python make_emission_intermediate.py"
        " bilan_maille_1k_v76_2016.txt"
        " 12/01/2016 12/02/2016 02 03 --poll pm2.5 --dt 1"))
    parser.add_argument("path", help="Path to the global emission inventory")
    parser.add_argument("date_start", help="Start date in UTC: mm/dd/yyyy")
    parser.add_argument("date_end", help="End date in UTC: mm/dd/yyyy")
    parser.add_argument('snaps', nargs='+', type=str, help="List of string stating which snap number as 02, 07, 08")
    parser.add_argument("--poll", help="String stating which pollutants to choose: pm10, pm2.5, so2, bap, ch4, nh3, nox, covnm, co", default="pm10")
    parser.add_argument("--pref", help="Prefix for the output file", default="PMS")
    parser.add_argument("--dt", type=int, help="Number of hours in between each input for pollutants", default=1)
    parser.add_argument("--vert_offset", type=int, help="Vertcial offset, in case using two pollutants", default=0)
    parser.add_argument("--out_path", type=str, help="path to save output files", default="./")
    args = parser.parse_args()
    
    # Some global parameters
    ## Prefix for output file
    prefix = args.pref
    ## Sequence of date to process
    ### Force the format to read to be sure not to get any confusion !
    date_start = pd.to_datetime(args.date_start, format="%m/%d/%Y")
    date_end = pd.to_datetime(args.date_end, format="%m/%d/%Y")
    tz = "Europe/Paris"
    # Define wrf vertical levels
    vert_levs = [100, 200, 300, 500, 700, 1000, 2000, 3000, 5000, 7000, 1e4, 1.25e4, 1.5e4, 1.75e4, 2e4, 2.25e4, 2.5e4, 3e4, 3.5e4, 4e4, 4.5e4, 5e4, 5.5e4]

    # Read the pollutants
    df = read_emiss2016_csv(args.path)
    # Compute lower left lon and lat
    startlon, startlat = lam93_28latlon(df.xcentre.min(0), df.ycentre.min(0))
    print(f"   startlon={startlon}")
    print(f"   startlat={startlat}")

    # Make emission for all snaps ! Useful anyway to aggregate them
    allsnaps_dict = make_resp_weigths_forall_snaps(df, pol=args.poll)

    # Plot them just for safety reasons :
    for snap in args.snaps:
        if snap != "all":
            print("bob")
            # disbale for when X11 is down
            #plot_emissfield(allsnaps_dict[snap], snap, poll=args.poll)

    # Important Note : Dates given for this script correspond to WRF dates.
    # However, the deconvolution are in local time.
    dates = pd.date_range(start=date_start, end=date_end, freq=f"{args.dt}H")
    for date in dates:
        datestr = date.strftime("%Y-%m-%d_%H")
        print(f"****************************************")
        print(f"Processing {datestr} in WRF time (UTC+00)")
        date = date.tz_localize(tz="UTC")
        local_date = date.tz_convert(tz=tz)
        print(f"Local time in {tz}: {local_date}") 
        # Create the output file
        intermediate.write_met_init(prefix, datestr)


        for k,snap in enumerate(args.snaps):
            print(f"Processing snap {snap}")
            # Compute the field
            if snap == "all":
                f = make_all_snaps_field(allsnaps_dict, local_date)
            else:
                f = make_hourly_emission(allsnaps_dict[snap], local_date, snap)
            print(f"  max poll value={f.max()} ug/m^2/s")

            # Write it to a temporary file
            #slab = np.swapaxes(f, 0, 1)
            slab = f
            #slab = np.random.rand(slab.shape[0], slab.shape[1])
            version = int(5)
            shape = slab.shape
            nx = int(shape[0])
            ny = int(shape[1])
            # Compute lower left lon and lat
            endlon, endlat = lam93_28latlon(df.ycentre.max(0), df.xcentre.max(0))
            # Is it in latlon or in Lambert ??
            deltalon = 999
            deltalat = 999
            ounit = int(10)
            iproj = int(1) 
            # Projection codes for proj_info structure:
            # integer, public, parameter  :: PROJ_LATLON = 0
            # integer, public, parameter  :: PROJ_LC = 1
            # integer, public, parameter  :: PROJ_PS = 2
            # integer, public, parameter  :: PROJ_PS_WGS84 = 102
            # integer, public, parameter  :: PROJ_MERC = 3
            # integer, public, parameter  :: PROJ_GAUSS = 4
            # integer, public, parameter  :: PROJ_CYL = 5
            # integer, public, parameter  :: PROJ_CASSINI = 6
            # integer, public, parameter  :: PROJ_ALBERS_NAD83 = 105 
            # integer, public, parameter  :: PROJ_ROTLL = 203
            nlats = int(0)
            xfcst = int(0)
            xlvl = int(vert_levs[k] + args.vert_offset) 
            dx = float(1)
            dy = float(1) 
            xlonc = float(3)
            truelat1 = float(44)
            truelat2 = float(49)
            earth_radius = float(6378.137)
            is_wind_grid_rel = False
            startloc = "SWCORNER"
            field = "PMS"
            units = "mug/sec/m2"
            map_source = "Atmo AURA"
            desc = "Emission Inventory provided by Atmo AURA"

            intermediate.write_next_met_field(
                version=version,
                nx=nx,
                ny=ny,
                iproj=iproj,
                xfcst=xfcst,
                xlvl=xlvl,
                startlat=startlat,
                startlon=startlon,
                deltalat=deltalat,
                deltalon=deltalon,
                dx=dx,
                dy=dy,
                xlonc=xlonc,
                truelat1=truelat1,
                truelat2=truelat2,
                earth_radius=earth_radius,
                slab=slab,
                is_wind_grid_rel=is_wind_grid_rel,
                startloc=startloc,
                field=field,
                hdate=datestr,
                units=units,
                map_source=map_source,
                desc=desc,
            )

        # Close the output file
        intermediate.write_met_close()

        shutil.move(f"{prefix}:{datestr}", f"{args.out_path}{prefix}:{datestr}")
