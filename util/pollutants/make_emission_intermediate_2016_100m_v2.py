import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
# Add folder of this file to python path to automatically include the fortran auto generated module
sys.path.append(os.path.dirname(__file__))
from WPSUtils import intermediate
from pyproj import Proj, transform
import argparse
import warnings
import shutil

import matplotlib as mpl
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.colors import ListedColormap
from matplotlib.gridspec import GridSpec
mpl.rcParams['axes.titlesize'] = 24
mpl.rcParams['axes.labelsize'] = 24
mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.markersize'] = 10
mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rcParams['legend.fontsize'] = 20
mpl.rcParams['figure.titlesize'] = 24

# The following two lines are used to have all text looking alike Latex fonts !
# https://stackoverflow.com/questions/11367736/matplotlib-consistent-font-using-latex
mpl.rcParams['mathtext.fontset'] = 'stix'
mpl.rcParams['font.family'] = 'STIXGeneral'

def TNO_monthly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 12)), index=indxs, columns=np.arange(1,13))
    df.loc['01'] = [1.20, 1.15, 1.05, 1.00, 0.90,
                    0.85, 0.80, 0.87, 0.95, 1.00, 1.08, 1.15]
    df.loc['02'] = [1.70, 1.50, 1.30, 1.00, 0.70,
                    0.40, 0.20, 0.40, 0.70, 1.05, 1.40, 1.65]
    df.loc['03'] = [1.10, 1.08, 1.05, 1.00, 0.95,
                    0.90, 0.93, 0.95, 0.97, 1.00, 1.02, 1.05]
    df.loc['04'] = [1.02, 1.02, 1.02, 1.02, 1.02,
                    1.02, 1.00, 0.84, 1.02, 1.02, 1.02, 0.90]
    df.loc['05'] = [1.20, 1.20, 1.20, 0.80, 0.80,
                    0.80, 0.80, 0.80, 0.80, 1.20, 1.20, 1.20]
    df.loc['06'] = [0.95, 0.96, 1.02, 1.00, 1.01,
                    1.03, 1.03, 1.01, 1.04, 1.03, 1.01, 0.91]
    df.loc['07'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07a'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07b'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07c'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['08'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                    1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['09'] = [1.00, 1.00, 1.00, 1.00, 1.00,
                    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10'] = [0.45, 1.30, 2.35, 1.70, 0.85,
                    0.85, 0.85, 1.00, 1.10, 0.65, 0.45, 0.45]
    return df.multiply(df.shape[1]/df.sum(1),'index')


def TNO_dayly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 7)), index=indxs, columns=np.arange(1,8))
    df.loc['01']    = [1.06, 1.06, 1.06, 1.06, 1.06, 0.85, 0.85]
    df.loc['02']    = [1.08, 1.08, 1.08, 1.08, 1.08, 0.80, 0.80]
    df.loc['03']    = [1.08, 1.08, 1.08, 1.08, 1.08, 0.80, 0.80]
    df.loc['04']    = [1.02, 1.02, 1.02, 1.02, 1.02, 1.02, 1.00]
    df.loc['05']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['06']    = [1.20, 1.20, 1.20, 1.20, 1.20, 0.50, 0.50]
    df.loc['07']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07a']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07b']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07c']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['08']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['09']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10']   = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    return df.multiply(df.shape[1]/df.sum(1),'index')

def TNO_hourly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 24)), index=indxs, columns=np.arange(0, 24))
    df.loc['01']    = [0.79, 0.72, 0.72, 0.71, 0.74, 0.80, 0.92, 1.08, 1.19, 1.22, 1.21, 1.21,
                       1.17, 1.15, 1.14, 1.13, 1.10, 1.07, 1.04, 1.02, 1.02, 1.01, 0.96, 0.88]
    df.loc['02']    = [0.38, 0.36, 0.36, 0.36, 0.37, 0.50, 1.19, 1.53, 1.57, 1.56, 1.35, 1.16,
                       1.07, 1.06, 1.00, 0.98, 0.99, 1.12, 1.41, 1.52, 1.39, 1.35, 1.00, 0.42]
    df.loc['03']    = [0.75, 0.75, 0.78, 0.82, 0.88, 0.95, 1.02, 1.09, 1.16, 1.22, 1.28, 1.30,
                       1.22, 1.24, 1.25, 1.16, 1.08, 1.01, 0.95, 0.90, 0.85, 0.81, 0.78, 0.75]
    df.loc['04']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['05']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['06']    = [0.50, 0.35, 0.10, 0.10, 0.20, 0.50, 0.75, 1.25, 1.40, 1.50, 1.50, 1.50,
                       1.50, 1.50, 1.50, 1.50, 1.50, 1.40, 1.25, 1.10, 1.00, 0.90, 0.80, 0.70]
    df.loc['07']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07a']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07b']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07c']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['08']    = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['09']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10']   = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    return df.multiply(df.shape[1]/df.sum(1),'index')



def make_hourly_emission(total_emiss, date, snap:str):
    """
    Arguments:
        total_emiss {np.array} : yearly emission in tons per year in a grid 
            cell of a square kilometer.
        date {pd.Timestamp} : date at which to compute the deconvolution.
        snap {str} : string stating which snap should be used for deconvolution
    """
    mon_fac = TNO_monthly().loc[snap][date.month] # .month from 1 to 12
    day_fac = TNO_dayly().loc[snap][date.dayofweek + 1] # dayofweek from 0 to 6
    hr_fac = TNO_hourly().loc[snap][date.hour]
    date_str = date.strftime("%m/%d/%Y %H:%M")
    print(f"Sn{snap} at {date_str}: hr_fac : {hr_fac:1.2f} | day_fac = {day_fac:1.2f} | mth_fac= {mon_fac:1.2f}")
    #  emissions / (year -> seconds [/ 31536000]) * (ton -> ug [* 10^12]) / (m2 in emission inventory cell [/ 1e6])
    emis_per_sec = total_emiss / 31.536e6 * 1e12 * hr_fac * day_fac * mon_fac / 1e4
    return emis_per_sec

def lam93_28latlon(x, y):
    """Project x and y from Lambert 93 to LatLon
    
    Arguments:
        x {float} -- 
        y {float} -- 
    
    Returns:
        float -- Longitude
        float -- Latitude
    """
    inProj = Proj(init='epsg:2154')  # 2154 stands for Lambert 93
    outProj = Proj(init='epsg:4326')  # 4326 stands for LatLon
    lon, lat = transform(inProj, outProj, x, y)
    return lon, lat

def read_polls_csv(path):
    csv = pd.read_csv(path, sep=";", header=None)
    return csv


def read_emiss2016_csv(path:str):
    """Read the pollutants emission inventory from 
    Atmo AURA produced in 2020 for 2016 at km.
    
    Arguments:
        path {str} -- Path to the csv file
    
    Returns:
        pandas.Dataframe -- Dataframe containing the whole dataset.
    """
    print(f"-> Reading the emission inventory {path}")
    df = pd.read_csv(path, sep=";", header=0, converters={0:float, 1: float, 2:str, 3:str, 4:str, 5:str, 6:str, 7:float, 8:float})
    # Rename the emission column to something less confusing
    df.rename(columns={"sum":"emiss"}, inplace=True)
    # Strip whitespaces from snap_ids
    df['id_snap3'] = df['id_snap3'].str.strip()
    # Convert t to True and others to False
    df["chauffage"] = df["chauffage"] == "t"
    df["chauffage_bois"] = df["chauffage_bois"] == "t"
    # Set xcentre and ycentre as indexes
    # df.set_index(["xcentre","ycentre"],inplace=True)
    return df

def read_emiss2020_for_2016_100m(path:str):
    """Read the csv file produced after concatenation of
    snap7 to the original emission file.
    
    Arguments:
        path {str} -- Path to the csv file
    
    Returns:
        pandas.Dataframe -- Dataframe containing the whole dataset.
    """
    print(f"-> Reading the emission inventory {path}")
    # 0 index, 
    df = pd.read_csv(path, sep=",", header=0, converters={0:float, 1: float, 2:str, 3:str, 4:str, 5:str, 6:str}, nrows=None)
    # Cast x and y to integers to avoid rounding errors
    df.id_maille = df.id_maille.astype(int)
    #df.x = df.x.astype(int)
    #df.y = df.y.astype(int)   
    # Strip whitespaces from snap_ids
    df['id_snap3'] = df['id_snap3'].str.strip()
    # Set xcentre and ycentre as indexes
    # df.set_index(["xcentre","ycentre"],inplace=True)
    return df
    

def create_field_from_df(df, snap_id, pol="pm10"):
    # Get global min and max values of staggering
    
    ## Method 1 but seems a bit bugguy !
    xval = np.unique(df.x_centre)
    yval = np.unique(df.y_centre)

    
    # Create a dataframe with index xloc and yloc and ampty values
    iterables =  [xval, yval]
    ind = pd.MultiIndex.from_product(iterables, names=['x_centre', 'y_centre'])
    dfemp = pd.DataFrame(index=ind)
    # Make emissions
    df_pol = df.loc[df['pol'] == pol]
    emiss_snap = df_pol.loc[df.id_snap3.str.startswith(snap_id)]
    emiss_snap = emiss_snap.groupby(['x_centre','y_centre']).sum().val
    tmp = dfemp.join(emiss_snap)
    tmp.fillna(0, inplace=True)
    field = np.reshape(tmp.values, (len(xval),len(yval)))
    return field.T, xval, yval
    
def make_resp_weigths_forall_snaps(df, pol):
    """ Read the dataframe as created by read_emiss and create 
    a field aggregated for all snaps.

    """
    print(f"Preparing snaps to make the all_snaps field !")
    # make an empty dictionnary to store results
    res_dict= {}
    # Loop over all the snap sectors
    indxs = ["01", "02", "03", "04", "05", "06",
             "07", "08", "09", "10"]
    for idx in indxs:
        print(f"Processing {idx}")
        field, xval, yval = create_field_from_df(df,  idx, pol)
        res_dict[idx] = field 
    return res_dict, xval, yval




def make_all_snaps_field(allsnaps_dict, date):
    """ Function to create a field with all snaps being aggregated together.
    It takes as input a dict with each snap and its corresponding yearly
    emission field and a date.
    """
    snp_idxs = list(allsnaps_dict.keys())
    all_emiss = np.zeros(allsnaps_dict[snp_idxs[0]].shape)
    for idx in snp_idxs:
        hr_emis_snap = make_hourly_emission(allsnaps_dict[idx], date, idx)
        print(f"{idx} : sum = {hr_emis_snap.sum()}")
        all_emiss = all_emiss + hr_emis_snap 
    print(f"all final : sum = {all_emiss.sum()}")
    return all_emiss

def plot_emissfield(xval, yval, field, code, poll="PM10"):
    # Define the max value for cmap
    if np.max(field) != 0:
        vmax = np.percentile(field[field!=0],90)
    else:
        vmax = 0
        
    fig, ax = plt.subplots(figsize=(12,8))
    pcm = ax.pcolormesh(xval, yval, field, vmax=vmax, cmap="magma")
    ax.axes.xaxis.set_visible(False)
    ax.axes.yaxis.set_visible(False)
    fig.colorbar(pcm, label=f"Emission of {code} in $t/year$")
    ax.set_title(f"{poll} : {code} => total {field.sum():.5f}")
    fig.savefig(f"{poll}_{code}.png", dpi=150)
    plt.close()
    return None
    

# ===============================================================================
# Start of the script itself
# ===============================================================================

if __name__ == "__main__":
    # Parse arguments fo the python script
    parser = argparse.ArgumentParser(epilog=(
        "Example: python make_emission_intermediate.py"
        " bilan_maille_1k_v76_2016.txt"
        " 12/01/2016 12/02/2016 02 03 --poll pm2.5 --dt 1"))
    parser.add_argument("path", help="Path to the global emission inventory")
    parser.add_argument("date_start", help="Start date in UTC: mm/dd/yyyy")
    parser.add_argument("date_end", help="End date in UTC: mm/dd/yyyy")
    parser.add_argument('snaps', nargs='+', type=str, help="List of string stating which snap number as 02, 07, 08")
    parser.add_argument("--poll", help="String stating which pollutants to choose: pm10, pm2.5, so2, bap, ch4, nh3, nox, covnm, co", default="pm10")
    parser.add_argument("--pref", help="Prefix for the output file", default="PMS")
    parser.add_argument("--dt", type=int, help="Number of hours in between each input for pollutants", default=1)
    parser.add_argument("--vert_offset", type=int, help="Vertcial offset, in case using two pollutants", default=0)
    parser.add_argument("--out_path", type=str, help="path to save output files", default="./")
    args = parser.parse_args()
    
    # Some global parameters
    ## Prefix for output file
    prefix = args.pref
    ## Sequence of date to process
    ### Force the format to read to be sure not to get any confusion !
    date_start = pd.to_datetime(args.date_start, format="%m/%d/%Y")
    date_end = pd.to_datetime(args.date_end, format="%m/%d/%Y")
    tz = "Europe/Paris"
    # Define wrf vertical levels
    vert_levs = np.arange(20) + 1
    # Read the pollutants
    df = read_emiss2020_for_2016_100m(args.path)
    # Compute lower left lon and lat
    startlon, startlat = lam93_28latlon(df.x_centre.min(0), df.y_centre.min(0))
    print(f"   startlon={startlon}")
    print(f"   startlat={startlat}")

    # Make emission for all snaps ! Useful anyway to aggregate them
    allsnaps_dict, xval, yval = make_resp_weigths_forall_snaps(df, args.poll)

    # Plot them just for safety reasons :
    for snap in args.snaps:
        if snap != "all":
            plot_emissfield(xval, yval, allsnaps_dict[snap], snap, poll=args.poll)
        else:
            sum_all = np.zeros(allsnaps_dict["01"].shape)
            for snap_idx in allsnaps_dict.keys():
                sum_all += allsnaps_dict[snap_idx]
            plot_emissfield(xval, yval, sum_all, "all", poll=args.poll)

    # Important Note : Dates given for this script correspond to WRF dates.
    # However, the deconvolution are in local time.
    dates = pd.date_range(start=date_start, end=date_end, freq=f"{args.dt}H")
    for kdate,date in enumerate(dates):
        datestr = date.strftime("%Y-%m-%d_%H")
        print(f"****************************************")
        print(f"Processing {datestr} in WRF time (UTC+00)")
        date = date.tz_localize(tz="UTC")
        local_date = date.tz_convert(tz=tz)
        print(f"Local time in {tz}: {local_date}") 
        # Create the output file
        intermediate.write_met_init(prefix, datestr)


        for k,snap in enumerate(args.snaps):
            print(f"Processing snap {snap}")
            
            # Compute the field
            if snap == "all":
                f = make_all_snaps_field(allsnaps_dict, local_date)
            else:
                f = make_hourly_emission(allsnaps_dict[snap], local_date, snap)
            
            
            # Output some info about the slab
            print(f"  max poll value={f.max()} ug/m^2/s")
            if f.max() > 0:
                perc90 = np.percentile(f[f>0],90)
                print(f"  90th perc poll value={perc90} ug/m^2/s")
                print(f"  sum = {f.sum()} ug/m^2/s")
                print(f"  min = {f.min()} ug/m^2/s")
            
            # TO BE REMOVED
            # USED FOR ASSESSING MASS CONSERVATIOn ONLY
            #f[0:150,:] = 0
            #f[-150:,:] = 0
            #f[:,-150:] = 0
            #f[:,0:150] = 0
            
            if kdate % 24 == 0:
                plot_emissfield(xval, yval, f, f"{snap}_{datestr}UTC", poll=args.poll)

            # Write it to a temporary file
            slab = np.swapaxes(f, 0, 1)
            #slab = f.reshape(f.shape, order="F")

            version = int(5)
            shape = slab.shape
            nx = int(shape[0])
            ny = int(shape[1])
            deltalon = 999
            deltalat = 999
            ounit = int(10)
            iproj = int(1) 
            # Projection codes for proj_info structure:
            # integer, public, parameter  :: PROJ_LATLON = 0
            # integer, public, parameter  :: PROJ_LC = 1
            # integer, public, parameter  :: PROJ_PS = 2
            # integer, public, parameter  :: PROJ_PS_WGS84 = 102
            # integer, public, parameter  :: PROJ_MERC = 3
            # integer, public, parameter  :: PROJ_GAUSS = 4
            # integer, public, parameter  :: PROJ_CYL = 5
            # integer, public, parameter  :: PROJ_CASSINI = 6
            # integer, public, parameter  :: PROJ_ALBERS_NAD83 = 105 
            # integer, public, parameter  :: PROJ_ROTLL = 203
            nlats = int(0)
            xfcst = int(0)
            xlvl = int(vert_levs[k] + args.vert_offset) 
            dx = float(0.1)
            dy = float(0.1) 
            xlonc = float(3)
            truelat1 = float(44)
            truelat2 = float(49)
            earth_radius = float(6378.137)
            is_wind_grid_rel = False
            startloc = "SWCORNER"
            field = "PMS"
            units = "mug/sec/m2"
            map_source = "Atmo AURA"
            desc = f"Emission Inventory snap {snap} poll {args.poll}"

            intermediate.write_next_met_field(
                version=version,
                nx=nx,
                ny=ny,
                iproj=iproj,
                xfcst=xfcst,
                xlvl=xlvl,
                startlat=startlat,
                startlon=startlon,
                deltalat=deltalat,
                deltalon=deltalon,
                dx=dx,
                dy=dy,
                xlonc=xlonc,
                truelat1=truelat1,
                truelat2=truelat2,
                earth_radius=earth_radius,
                slab=slab,
                is_wind_grid_rel=is_wind_grid_rel,
                startloc=startloc,
                field=field,
                hdate=datestr,
                units=units,
                map_source=map_source,
                desc=desc,
            )

        # Close the output file
        intermediate.write_met_close()

        shutil.move(f"{prefix}:{datestr}", f"{args.out_path}{prefix}:{datestr}")
