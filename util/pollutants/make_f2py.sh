#!/bin/bash
# Usage: Compile the fortran function make_intermediate.f90 as a python library

if  [ "$1" = 'little' ]; then
    sed -i -E "s/convert.*?,/convert='little_endian',/" make_intermediate.f90
elif [ "$1" = 'big' ]; then
    sed -i -E "s/convert.*?,/convert='big_endian',/" make_intermediate.f90
else
    echo "!!!!! ERROR : Endianness should be supplied as an argument !!!!!"
    echo "Please provide 'big' or 'little' "
    echo "Exiting program"
    exit
fi

f2py -c -m WPSUtils intermediate-duda.F90
