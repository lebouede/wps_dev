import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
# Add folder of this file to python path to automatically include the fortran auto generated module
sys.path.append(os.path.dirname(__file__))
from WPSUtils import intermediate
from pyproj import Proj, transform
import argparse
import warnings


def TNO_monthly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 12)), index=indxs, columns=np.arange(1,13))
    df.loc['01'] = [1.20, 1.15, 1.05, 1.00, 0.90,
                    0.85, 0.80, 0.87, 0.95, 1.00, 1.08, 1.15]
    df.loc['02'] = [1.70, 1.50, 1.30, 1.00, 0.70,
                    0.40, 0.20, 0.40, 0.70, 1.05, 1.40, 1.65]
    df.loc['03'] = [1.10, 1.08, 1.05, 1.00, 0.95,
                    0.90, 0.93, 0.95, 0.97, 1.00, 1.02, 1.05]
    df.loc['04'] = [1.02, 1.02, 1.02, 1.02, 1.02,
                    1.02, 1.00, 0.84, 1.02, 1.02, 1.02, 0.90]
    df.loc['05'] = [1.20, 1.20, 1.20, 0.80, 0.80,
                    0.80, 0.80, 0.80, 0.80, 1.20, 1.20, 1.20]
    df.loc['06'] = [0.95, 0.96, 1.02, 1.00, 1.01,
                    1.03, 1.03, 1.01, 1.04, 1.03, 1.01, 0.91]
    df.loc['07a'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07b'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['07c'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                     1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['08'] = [0.88, 0.92, 0.98, 1.03, 1.05,
                    1.06, 1.01, 1.02, 1.06, 1.05, 1.01, 0.93]
    df.loc['09'] = [1.00, 1.00, 1.00, 1.00, 1.00,
                    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10'] = [0.45, 1.30, 2.35, 1.70, 0.85,
                    0.85, 0.85, 1.00, 1.10, 0.65, 0.45, 0.45]
    return df.multiply(df.shape[1]/df.sum(1),'index')


def TNO_dayly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 7)), index=indxs, columns=np.arange(1,8))
    df.loc['01']    = [1.06, 1.06, 1.06, 1.06, 1.06, 0.85, 0.85]
    df.loc['02']    = [1.08, 1.08, 1.08, 1.08, 1.08, 0.80, 0.80]
    df.loc['03']    = [1.08, 1.08, 1.08, 1.08, 1.08, 0.80, 0.80]
    df.loc['04']    = [1.02, 1.02, 1.02, 1.02, 1.02, 1.02, 1.00]
    df.loc['05']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['06']    = [1.20, 1.20, 1.20, 1.20, 1.20, 0.50, 0.50]
    df.loc['07a']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07b']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['07c']   = [1.02, 1.06, 1.08, 1.10, 1.14, 0.81, 0.79]
    df.loc['08']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['09']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10']   = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    return df.multiply(df.shape[1]/df.sum(1),'index')

def TNO_hourly():
    indxs = ["01", "02", "03", "04", "05", "06",
             "07a", "07b", "07c", "08", "09", "10"]
    df = pd.DataFrame(np.zeros((12, 24)), index=indxs, columns=np.arange(0, 24))
    df.loc['01']    = [0.79, 0.72, 0.72, 0.71, 0.74, 0.80, 0.92, 1.08, 1.19, 1.22, 1.21, 1.21,
                       1.17, 1.15, 1.14, 1.13, 1.10, 1.07, 1.04, 1.02, 1.02, 1.01, 0.96, 0.88]
    df.loc['02']    = [0.38, 0.36, 0.36, 0.36, 0.37, 0.50, 1.19, 1.53, 1.57, 1.56, 1.35, 1.16,
                       1.07, 1.06, 1.00, 0.98, 0.99, 1.12, 1.41, 1.52, 1.39, 1.35, 1.00, 0.42]
    df.loc['03']    = [0.75, 0.75, 0.78, 0.82, 0.88, 0.95, 1.02, 1.09, 1.16, 1.22, 1.28, 1.30,
                       1.22, 1.24, 1.25, 1.16, 1.08, 1.01, 0.95, 0.90, 0.85, 0.81, 0.78, 0.75]
    df.loc['04']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['05']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['06']    = [0.50, 0.35, 0.10, 0.10, 0.20, 0.50, 0.75, 1.25, 1.40, 1.50, 1.50, 1.50,
                       1.50, 1.50, 1.50, 1.50, 1.50, 1.40, 1.25, 1.10, 1.00, 0.90, 0.80, 0.70]
    df.loc['07a']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07b']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['07c']   = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['08']    = [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20,
                       1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44]
    df.loc['09']    = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    df.loc['10']   = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
                       1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
    return df.multiply(df.shape[1]/df.sum(1),'index')


def make_hourly_emission(total_emiss, date, snap):
    mon_fac = TNO_monthly().loc[snap][date.month]
    day_fac = TNO_dayly().loc[snap][date.dayofweek]
    hr_fac = TNO_hourly().loc[snap][date.hour]
    print(f"hourly fac : {hr_fac} | daily fac = {day_fac} | monthly fac= {mon_fac}")
    #  emissions / (year -> hours [/ 8760]) * (ton -> kg [* 1000]) / (m2 in emission inventory cell [/ 1e6])
    hr_emis = total_emiss / 8.76 * hr_fac * day_fac * mon_fac / 1e6
    return hr_emis

def lam93_28latlon(x, y):
    """Project x and y from Lambert 93 to LatLon
    
    Arguments:
        x {float} -- 
        y {float} -- 
    
    Returns:
        float -- Longitude
        float -- Latitude
    """
    inProj = Proj(init='epsg:2154')  # 2154 stands for Lambert 93
    outProj = Proj(init='epsg:4326')  # 4326 stands for LatLon
    lon, lat = transform(inProj, outProj, x, y)
    return lon, lat

def read_polls_csv(path):
    csv = pd.read_csv(path, sep=";", header=None)
    return csv


def read_emiss2016_csv(path:str):
    """Read the pollutants emission inventory from 
    Atmo AURA produced in 2020 for 2016.
    
    Arguments:
        path {str} -- Path to the csv file
    
    Returns:
        pandas.Dataframe -- Dataframe containing the whole dataset.
    """
    print(f"-> Reading the emission inventory {path}")
    df = pd.read_csv(path, sep=";", header=0, converters={0:float, 1: float, 2:str, 3:str, 4:str, 5:str, 6:str, 7:float, 8:float})
    # Rename the emission column to something less confusing
    df.rename(columns={"sum":"emiss"}, inplace=True)
    # Strip whitespaces from snap_ids
    df['id_snap3'] = df['id_snap3'].str.strip()
    # Convert t to True and others to False
    df["chauffage"] = df["chauffage"] == "t"
    df["chauffage_bois"] = df["chauffage_bois"] == "t"
    # Set xcentre and ycentre as indexes
    # df.set_index(["xcentre","ycentre"],inplace=True)
    return df

def df2emissfield(df, pol:str="pm10",codes:list=["02"],debugflag=False):
    print(f"Select the desired pollutants : {pol}")
    # Tests idx and idy to hceck that everything seems all right
    df_pol = df.loc[df['pol'] == pol]
    if debugflag:
        idx = 842000
        idy = 6518000
        print(df_pol[np.logical_and(df.xcentre == idx, df.ycentre==idy)])
    res = []
    # Compute nx and ny once and for all
    nx = len(df.xcentre.unique())
    ny = len(df.ycentre.unique())
    for k,code in enumerate(codes):
        print(f"Processing {code}")
        # Select all lines with the desired code pattern
        df_code = df_pol.loc[df["id_snap3"].str.startswith(code)]
        df_code.set_index(["xcentre","ycentre"],inplace=True)
        if debugflag:
            print(df_code.loc[(idx, idy)])
        # Group by on xentre and ycentre and then sum to take
        # care of snaps sublevels: Exemple code "02"
        # xcentre ycentre code emiss
        # 1 1   020100  100
        # 1 1   020200  50
        total_emiss = df_code.groupby(["xcentre","ycentre"]).sum()
        # Create a dataframe with index xloc and yloc and ampty values
        iterables =  [np.arange(df.xcentre.unique().min(),df.xcentre.unique().max()+1e3, 1e3),
                      np.arange(df.ycentre.unique().min(),df.ycentre.unique().max()+1e3, 1e3)]
        ind = pd.MultiIndex.from_product(iterables, names=['xcentre', 'ycentre'])
        dfemp = pd.DataFrame(index=ind)
        # merge the dataframes
        if debugflag:
            print(total_emiss.loc[(idx,idy)]) 
        tmp = dfemp.join(total_emiss)
        tmp.fillna(0, inplace=True)
        res.append(np.reshape(tmp.emiss.values, (nx, ny)))
    return res

def plot_emissfield(field, code, poll):
    fig, ax = plt.subplots()
    pcm = ax.pcolormesh(np.swapaxes(field,0,1))
    fig.colorbar(pcm, label=f"Emission of {code} in $t/km^2$")
    ax.set_title(f"Surface emission for {code}")
    fig.savefig(f"{poll}_{code}.png")
    return None

def read_polls_as_field(df, snap):
    warnings.warn("deprecated", DeprecationWarning)
    corresp_dict={'CSa':np.arange(4, 18),
                  'S1':4,
                  'S2':5,
                  'S3':6,
                  'S4':7,
                  'S6':9,
                  'S5':8,
          'S7a':10,
          'S8':11,
          'S9':12}
    if "CS" in snap:
        poll = df[corresp_dict[snap]].sum(1)
    else:
        poll = df[corresp_dict[snap]]
    # Replace Nans by zeros
    poll.fillna(0, inplace=True)
    # Make a big field of it
    f = np.reshape(poll.values, (np.unique(df[1]).size, np.unique(df[2]).size))
    return f


if __name__ == "__main__":
    # Parse arguments fo the python script
    parser = argparse.ArgumentParser(epilog=(
        "Example: python make_emission_intermediate.py"
        " bilan_maille_1k_v76_2016.txt"
        " 12/01/2016 12/02/2016 02 03 --poll pm2.5 --dt 1"))
    parser.add_argument("path", help="Path to the global emission inventory")
    parser.add_argument("date_start", help="Start date : mm/dd/yyyy")
    parser.add_argument("date_end", help="End date : mm/dd/yyyy")
    parser.add_argument('snaps', nargs='+', type=str, help="List of string stating which snap number as 02, 07, 0701 ")
    parser.add_argument("--poll", help="String stating which pollutants to choose: pm10, pm2.5, so2, bap, ch4, nh3, nox, covnm, co", default="pm10")
    parser.add_argument("--dt", type=int, help="Number of hours in between each input for pollutants", default=1)

    #parser.add_argument("snaps", help="List of string stating which snap number as SX where X in [1,2,3,4,5,6,7,8,9,10]")
    args = parser.parse_args()

    prefix = "PMS"

    # Sequence of date to process
    # Force the format to read to be sure not to get any confusion !
    date_start = pd.to_datetime(args.date_start, format="%m/%d/%Y")
    date_end = pd.to_datetime(args.date_end, format="%m/%d/%Y")

    # Define wrf vertical levels
    vert_levs = [100, 200, 300, 500, 700, 1000, 2000, 3000, 5000, 7000, 1e4, 1.25e4, 1.5e4, 1.75e4, 2e4, 2.25e4, 2.5e4, 3e4, 3.5e4, 4e4, 4.5e4, 5e4, 5.5e4]
    #vert_levs = np.arange(20)


    # Read the pollutants
    df = read_emiss2016_csv(args.path)
    # Compute lower left lon and lat
    startlon, startlat = lam93_28latlon(df.xcentre.min(0), df.ycentre.min(0))
    print(f"   startlon={startlon}")
    print(f"   startlat={startlat}")

    # Make emission fields fol selected poll and snap codes
    emiss = df2emissfield(df, pol=args.poll, codes=args.snaps)

    # Plot them just for safety reasons :
    for k,field in enumerate(emiss):
        print("bob")
        #plot_emissfield(field, args.snaps[k], poll=args.poll)

    dates = pd.date_range(start=date_start, end=date_end, freq=f"{args.dt}H")
    for date in dates:
        datestr = date.strftime("%Y-%m-%d_%H")
        print(f"****************************************")
        print(f"Processing {datestr}")

        # Create the output file
        intermediate.write_met_init(prefix, datestr)

        for k,snap_emiss in enumerate(emiss):
            print(f"Processing snap {args.snaps[k]}")

            # Compute the field
            f = make_hourly_emission(snap_emiss, date, args.snaps[k])
            print(f"  max poll value={f.max()} kg/area/hour")

            # Write it to a temporary file
            #slab = np.swapaxes(f, 0, 1)
            slab = f
            #slab = np.random.rand(slab.shape[0], slab.shape[1])
            version = int(5)
            shape = slab.shape
            nx = int(shape[0])
            ny = int(shape[1])
            # Compute lower left lon and lat
            endlon, endlat = lam93_28latlon(df.ycentre.max(0), df.xcentre.max(0))
            # Is it in latlon or in Lambert ??
            deltalon = abs(startlon-endlon)/nx
            deltalat = abs(endlat-startlat)/ny
            print(f"   deltalat={deltalat}")
            print(f"   deltalon={deltalon}")
            ounit = int(10)
            iproj = int(1) 
            # Projection codes for proj_info structure:
            # integer, public, parameter  :: PROJ_LATLON = 0
            # integer, public, parameter  :: PROJ_LC = 1
            # integer, public, parameter  :: PROJ_PS = 2
            # integer, public, parameter  :: PROJ_PS_WGS84 = 102
            # integer, public, parameter  :: PROJ_MERC = 3
            # integer, public, parameter  :: PROJ_GAUSS = 4
            # integer, public, parameter  :: PROJ_CYL = 5
            # integer, public, parameter  :: PROJ_CASSINI = 6
            # integer, public, parameter  :: PROJ_ALBERS_NAD83 = 105 
            # integer, public, parameter  :: PROJ_ROTLL = 203
            nlats = int(0)
            xfcst = int(0)
            xlvl = int(vert_levs[k])  # 200100 corresponds to surface level
            dx = float(1)
            dy = float(1) 
            xlonc = float(3)
            truelat1 = float(44)
            truelat2 = float(49)
            earth_radius = float(6378.137)
            is_wind_grid_rel = False
            startloc = "SWCORNER"
            field = "PMS"
            units = "kg/hour/m2"
            map_source = "Atmo AURA"
            desc = "Emission Inventory provided by Atmo AURA"

            intermediate.write_next_met_field(
                version=version,
                nx=nx,
                ny=ny,
                iproj=iproj,
                xfcst=xfcst,
                xlvl=xlvl,
                startlat=startlat,
                startlon=startlon,
                deltalat=deltalat,
                deltalon=deltalon,
                dx=dx,
                dy=dy,
                xlonc=xlonc,
                truelat1=truelat1,
                truelat2=truelat2,
                earth_radius=earth_radius,
                slab=slab,
                is_wind_grid_rel=is_wind_grid_rel,
                startloc=startloc,
                field=field,
                hdate=datestr,
                units=units,
                map_source=map_source,
                desc=desc,
            )

        # Close the output file
        intermediate.write_met_close()
