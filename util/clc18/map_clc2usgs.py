from osgeo import gdal
import numpy as np
import math
import sys

def reclassify(source,output):
    src_ds = gdal.Open(source)
    print("Reading original file !")
    data   = src_ds.ReadAsArray(0,0,src_ds.RasterXSize,src_ds.RasterYSize)
    nx,ny = data.shape
    driver = gdal.GetDriverByName("GTiff")
    print("Creating copy of original file !")
    dst_ds = driver.CreateCopy(output, src_ds, 0 , [ 'COMPRESS=LZW' ])
    map_dict = make_map_dict()
    for orig_val in map_dict.keys():
        print(f"Processing key {orig_val}")
        data[data == orig_val] = map_dict[orig_val]

    dst_ds.GetRasterBand(1).WriteArray(data)
    dst_ds.SetProjection(src_ds.GetProjection())
    dst_ds.SetGeoTransform(src_ds.GetGeoTransform())

    driver = None
    dst_ds = None
    src_ds = None

# Mapping CLC to USGS
#
# 111:  Continuous urban fabric		      		= 32: High Intensity Residential
# 112:  Discontinuous urban fabric	      		= 31: Low Intensity Residential
# 121:  Industrial or commercial units	      		= 33: Industrial or Commercial
# 122:  Road and rail networks and associated land	= 33: Industrial or Commercial
# 123:  Port areas	      				= 33: Industrial or Commercial
# 124:  Airports  	      				= 33: Industrial or Commercial
# 131:  Mineral extraction sites				= 19: Barren or Sparsely Vegetated
# 132:  Dump sites	      				= 19: Barren or Sparsely Vegetated
# 133:  Construction sites      				= 19: Barren or Sparsely Vegetated
# 141: Green urban areas       				= 7:  Grassland
# 142: Sport and leisure facilities			= 7:  Grassland
# 211: Non-irrigated arable land				= 2:  Dryland Cropland and Pasture
# 212: Permanently irrigated land			= 3:  Irrigated Cropland and Pasture
# 213: Rice fields					= 3:  Irrigated Cropland and Pasture
# 221: Vineyards						= 6:  Cropland/Woodland Mosaic
# 222: Fruit trees and berry plantations       		= 6:  Cropland/Woodland Mosaic
# 223: Olive groves			      		= 6:  Cropland/Woodland Mosaic
# 231: Pastures  			      		= 2:  Dryland Cropland and Pasture
# 241: Annual crops associated with permanent crops	= 6:  Cropland/Woodland Mosaic
# 242: Complex cultivation patterns			= 4:  Mixed Dryland/Irrigated Cropland and Pasture
# 243: Land principally occupied by agriculture,		= 5:  Cropland/Grassland Mosaic
#     with significant areas of natural vegetation
# 244: Agro-forestry areas				= 6:  Cropland/Woodland Mosaic
# 311: Broad-leaved forest				= 11: Deciduous Broadleaf Forest
# 312: Coniferous forest					= 14: Evergreen Needleleaf Forest
# 313: Mixed forest    					= 15: Mixed Forest
# 321: Natural grasslands				= 7:  Grassland
# 322: Moors and heathland				= 9:  Mixed Shrubland/Grassland
# 323: Sclerophyllous vegetation				= 9:  Mixed Shrubland/Grassland
# 324: Transitional woodland-shrub			= 9:  Mixed Shrubland/Grassland
# 331: Beaches, dunes, sands				= 19: Barren or Sparsely Vegetated
# 332: Bare rocks	      				= 19: Barren or Sparsely Vegetated
# 333: Sparsely vegetated areas				= 19: Barren or Sparsely Vegetated
# 334: Burnt areas	      				= 19: Barren or Sparsely Vegetated
# 335: Glaciers and perpetual snow			= 24: Snow or Ice
# 411: Inland marshes					= 17: Herbaceous Wetland
# 412: Peat bogs						= 17: Herbaceous Wetland
# 421: Salt marshes					= 17: Herbaceous Wetland
# 422: Salines						= 17: Herbaceous Wetland
# 423: Intertidal flats					= 17: Herbaceous Wetland
# 511: Water courses   					= 16: Water Bodies
# 512: Water bodies    					= 28: Water Bodies
# 521: Coastal lagoons 					= 28: Water Bodies
# 522: Estuaries       					= 16: Water Bodies
# 523: Sea and ocean   					= 16: Water Bodies
# 999: NO DATA   					= 99: Missing value in USGS
# 50: Unclassified water bodies				= 28: Water Bodies

def make_map_dict():
    newclass={}
    newclass[111]  = 32
    newclass[112]  = 31
    newclass[121]  = 33
    newclass[122]  = 33
    newclass[123]  = 33
    newclass[124]  = 33
    newclass[131]  = 19
    newclass[132]  = 19
    newclass[133]  = 19
    newclass[141] = 7
    newclass[142] = 7
    newclass[211] = 2
    newclass[212] = 3
    newclass[213] = 3
    newclass[221] = 6
    newclass[222] = 6
    newclass[223] = 6
    newclass[231] = 2
    newclass[241] = 6
    newclass[242] = 4
    newclass[243] = 5
    newclass[244] = 6
    newclass[311] = 11
    newclass[312] = 14
    newclass[313] = 15
    newclass[321] = 7
    newclass[322] = 9
    newclass[323] = 9
    newclass[324] = 9
    newclass[331] = 19
    newclass[332] = 19
    newclass[333] = 19
    newclass[334] = 19
    newclass[335] = 24
    newclass[411] = 17
    newclass[412] = 17
    newclass[421] = 17
    newclass[422] = 17
    newclass[423] = 17
    newclass[511] = 16
    newclass[512] = 28
    newclass[521] = 28
    newclass[522] = 16
    newclass[523] = 16
    newclass[999] = 0
    newclass[-32768] = 0
    #newclass[50] = 28
    return newclass

if len(sys.argv)==3:
  reclassify(sys.argv[1],sys.argv[2])
