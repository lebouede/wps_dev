import numpy as np
import argparse
import gdal
import matplotlib.pyplot as plt

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("tif", help="Path to tif file", type=str)
    args = parser.parse_args()

    print(f"Processing {args.tif}")
    ds = gdal.Open(args.tif)
    tile = np.array(ds.GetRasterBand(1).ReadAsArray()).astype(">u1")
    print(f"Min {tile.min()} and max {tile.max()}")
    shx, shy = tile.shape
    print(f"Tile shape is {tile.shape}")
    max_values = 99999
    if shx > max_values or shy > max_values:
        print("Tile is too big to be written in a single field. Reducing its size by cropping the end !")
        tile = tile[:min(shx,max_values), :min(shy,max_values)]
        print(f"Reduced tile shape is {tile.shape}")
    xi = 0
    yi = 0
    xf = xi + tile.shape[0]
    yf = yi + tile.shape[1]
    filename = f"{yi+1:05d}-{yf:05d}.{xi+1:05d}-{xf:05d}"
    print("write tile to file")
    tile.tofile(filename)

    print("Plot the field written into a tile !")
    plt.imshow(tile)
    plt.savefig(f"{filename}.png")
    #tile.tofile(filename, format="%d")
