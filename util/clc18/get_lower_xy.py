from pyproj import Proj, transform

inProj = Proj(init='epsg:3035')
outProj = Proj(init='epsg:4326')

llx,lly = 9e5,9e5
urx,ury = 7400000.000, 5500000.000
nx=65000
ny=46000

llx2,lly2 = transform(inProj,outProj, llx, lly)
print(f"longitude {llx2} latitude {lly2}")

urx2,ury2 = transform(inProj,outProj, urx, ury)
print(f"longitude {urx2} latitude {ury2}")

print(f"dx = ({urx} - {llx}) / {nx}= {(urx - llx)/nx}")
print(f"dy = ({ury} - {lly}) / {ny}= {(ury - lly)/ny}")

gre_lat = 45.1667
gre_lon = 5.7167
gre_x, gre_y = transform(outProj, inProj, gre_lon, gre_lat)
print(f"Grenoble {gre_x} {gre_y}")
gre_dx = gre_x - llx
gre_dy = gre_y - lly
print(f"Grenoble index {gre_dx/100} {gre_dy/100}")
