# Download the data
Log in and download the tiff file: https://land.copernicus.eu/pan-european/corine-land-cover/clc2018?tab=download

# Unzip the file

# Convert tiff to latLon (EPSG:4326)
`gdalwarp -t_srs 'EPSG:4326' ./u2018_clc2018_v2020_20u1_raster100m/DATA/U2018_CLC2018_V2020_20u1.tif U2018_CLC2018_EPSG4326.tif`

# Map CLC to USGS33
`python map_clc2usgs.py U2018_CLC2018_EPSG4326.tif U2018_CLC2018_EPSG4326_USGS.tif`

# Make static tile
`python make_clc_static_tiles.py U2018_CLC2018_EPSG4326_USGS.tif`

_Note about endian_: you might be using WRF with big or little endian. One can use the check_endian.py script on static data that are known as functionnal to determine the endian. Depending on the result, one might need to change `">u2"` into `"<u2"`.

# Prepare your static folder
In your static folder:
- Create a new directory `landuse_clc18`
- Copy the tile within this directory
- Copy the index file within this directory

# Edit your GEOGRID.TBL
```
 ===============================
 name=LANDUSEF
     priority=2
     dest_type=categorical
     z_dim_name=land_cat
         landmask_water = clc18:16,28      # Calculate a landmask from this field
     interp_option = clc18:nearest_neighbor
     rel_path= clc18:specific/landuse_clc18/
 ===============================

```

# Edit your namelist options:
```
geog_data_res                       = 'clc18+default',
```