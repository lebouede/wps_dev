import numpy as np
import os
import re
import gdal
import pandas as pd
import argparse
import sys


def get_info_from_tif(tif: str):
    """get_info_from_tif Function that uses regular expression to extract
    informations for the binary intermadiate file from the gdal.Info method
    
    Parameters
    ----------
    tif : str
        path of the tif file
    
    Returns
    -------
    floats
        startlat, startlon, deltalat, deltalon
    """
    infos = gdal.Info(tif)
    pattern = "Lower\sLeft\D* (.*?),\s*(.*?)\)"
    startlon, startlat = re.search(pattern, infos).groups()
    pattern = "Pixel\sSize.*?\((.*?),\s*(.*?)\)"
    deltalat = re.search(pattern, infos).groups()[0]
    deltalon = deltalat
    return float(startlat), float(startlon), float(deltalat), float(deltalon)


def get_raw_albedo(tif):
    # Read the tif file
    ds = gdal.Open(tif)
    band = ds.GetRasterBand(1)
    alb = band.ReadAsArray()
    return alb


def reconstruct_modis_from_tifs(tifs):
    """this function reconstruct modis albedo fields using the current 
    date and a certain number of previous dates. At every time step, if a value
    of higher priority is found, it will replace the previous value. Orders of
    priority are defined as:
    
                    Missing values < No snow < Valid albedos

    Where 
    - Missing values
        - 101 : no decision
        - 111 : night
        - 150 : cloud
        - 151 : cloud detected as snow
        - 250 : missing
        - 251 : self-shadowing
    - No snow
        - 125 : land
        - 137 : inland water
        - 139 : ocean
        - 252 : landmask mismatch
        - 253 : BRDF failure
        - 254 : non production mask
    - Valid albedos
        - [1-100]

    If informations of the same priority level are compared, the most recent one
    will be kept.
    
    Parameters
    ----------
    tifs : list
        List of paths to tifs files where the first element is the newest
        and the last the oldest.
    
    Returns
    -------
    numpy.array
        Array of albedos where 255 corresponds to missing values, 0 to no snow
        and all values between 1 and 100 to valid albedos.
    """
    # First get the shape of the fields for initialization:
    shape = get_raw_albedo(tifs[0]).shape

    # Initalize everything
    albedo = np.full(shape, 0)  # Initialize albedo with zeroes everywhere
    age = np.full(
        shape, np.nan
    )  # age will keep track of how many days does the snow albedo has

    for k, tif in enumerate(tifs):
        # Get the albedo field
        tmp = get_raw_albedo(tif)
        # True : Modifiable | False : blocked
        mask = ~np.logical_and(albedo > 0, albedo <= 100)
        # Make the mask of newly available data
        new_data = np.logical_and(mask, tmp < 100)
        # Get valuable information
        age[new_data] = k  # add age information
        albedo[new_data] = tmp[new_data]  # add valid albedos
        print(f"*************** STEP {k} ***************")
        if k == 0:
            # At initialization add a missing value flag :
            albedo[
                np.logical_and(mask, np.isin(tmp, [101, 111, 150, 151, 250, 251]))
            ] = 255
            # Counters for statistics
            num_albs = age.size - np.isnan(age).sum()
            num_missing = (albedo == 255).sum()
            print(f"* Valid albedos  : {num_albs}")
            print(f"* Missing values : {num_missing}")
        else:
            new_num_albs = age.size - np.isnan(age).sum()
            new_num_missing = (albedo == 255).sum()
            if new_num_albs > num_albs:
                sign = "+"
            else:
                sign = ""
            print(
                f"* Valid albedos  : {new_num_albs} : {sign}{new_num_albs/num_albs*100 - 100:.3f}%"
            )
            print(
                f"* Missing values : {new_num_missing} : {new_num_missing/num_missing*100 - 100:.3f}%"
            )
            num_albs = new_num_albs
            num_missing = new_num_missing
        # Just a sanity check : no overwriting of newer values by older !
        # print(np.unique(age, return_counts=True))
    age[np.isnan(age)] = -1
    return albedo, age


if __name__ == "__main__":
    # Data to be downloaded from https://nsidc.org/data/MOD10A1/versions/6
    print(
        f"Script to transform a vrt of MODIS files changed as a tif into a temporary WPS file !"
    )

    # Add folder of this file to python path to automatically include the fortran auto generated module
    sys.path.append(os.path.dirname(__file__))
    import makeIntermediate

    # Parse arguments fo the python script
    parser = argparse.ArgumentParser()
    parser.add_argument("date_start", help="Start date : mm/dd/yyyy")
    parser.add_argument("date_end", help="End date : mm/dd/yyyy")
    parser.add_argument(
        "-a",
        "--albedo_lookback",
        dest="alb_lookback",
        help="Integer : number of days on which to lookback in time for albedo values",
        default=5,
        type=int,
    )
    args = parser.parse_args()

    # Sequence of date to process
    # Force the format to read to be sure not to get any confusion !
    date_start = pd.to_datetime(args.date_start, format="%m/%d/%Y")
    date_end = pd.to_datetime(args.date_end, format="%m/%d/%Y")
    print(
        f"Processing dates from {date_start.strftime('%B %d, %Y')} to {date_end.strftime('%B %d, %Y')}"
    )
    dates = pd.date_range(start=date_start, end=date_end)

    for date in dates:
        julday = date.dayofyear
        year = date.year
        datestr = date.strftime("%Y:%m:%d_%H:%M:%S")
        print(f"Processing {datestr}")
        # filename = f"MODIS_{julday}_{year}.tif"
        # albedo = get_modis_from_tif(filename)
        # Get the past dates for the X preceding days
        dateseq = pd.date_range(
            date + pd.Timedelta(-args.alb_lookback, unit="d"), date
        )[::-1]
        list_tifs = [
            f"MODIS_{d:03d}_{y}.tif" for d, y in zip(dateseq.dayofyear, dateseq.year)
        ]
        albedo, age = reconstruct_modis_from_tifs(list_tifs)
        # Print some infos
        print(
            f"albedo : min={albedo[albedo < 255].min()} max={albedo[albedo < 255].max()}"
        )
        print(f"age : min={age.min()} max={age.max()}")
        # Extract info from gdal_info
        startlat, startlon, deltalat, deltalon = get_info_from_tif(list_tifs[0])

        # Write it to a temporary file
        slab = np.flip(np.swapaxes(albedo, 0, 1), 1)
        version = int(5)
        shape = slab.shape
        nx = int(shape[0])
        ny = int(shape[1])
        # print(f"nx={nx} and ny={ny}")
        ounit = int(10)
        # MODIS is in equal-area sinusoidal projection (https://nsidc.org/data/MYD10A1/versions/6)
        # According to wikipedia, it is also called Sanson–Flamsteed or the
        # Mercator equal-area projection
        iproj = int(0)  # 0 Now in cylindrical equidistant
        nlats = int(30)
        xfcst = int(0)
        xlvl = int(200100)  # 200100 corresponds to surface level
        dx = float(489.823958781)
        dy = float(573.638082145)  # 463.31271653 ?
        xlonc = float(0)
        truelat1 = float(0)
        truelat2 = float(0)
        earth_radius = float(6378137)
        is_wind_grid_rel = False
        startloc = "SWCORNER"
        field = "ALB_MODIS"
        hdate = datestr
        units = "percentages"
        map_source = "modis"
        desc = "Albedo computed by satelite"

        # Set os env var to force big endian
        os.environ["GFORTRAN_CONVERT_UNIT"] = "big_endian"

        makeIntermediate.write_temporary(
            version=version,
            nx=nx,
            ny=ny,
            ounit=ounit,
            iproj=iproj,
            nlats=nlats,
            xfcst=xfcst,
            xlvl=xlvl,
            startlat=startlat,
            startlon=startlon,
            deltalat=deltalat,
            deltalon=deltalon,
            dx=dx,
            dy=dy,
            xlonc=xlonc,
            truelat1=truelat1,
            truelat2=truelat2,
            earth_radius=earth_radius,
            slab=slab,
            is_wind_grid_rel=is_wind_grid_rel,
            startloc=startloc,
            field=field,
            hdate=hdate,
            units=units,
            map_source=map_source,
            desc=desc,
        )

        # Write age field
        units = "days"
        field = "ALB_AGE"
        desc = "Age of the albedo data used in days"
        slab = np.flip(np.swapaxes(age, 0, 1), 1)
        makeIntermediate.write_temporary(
            version=version,
            nx=nx,
            ny=ny,
            ounit=ounit,
            iproj=iproj,
            nlats=nlats,
            xfcst=xfcst,
            xlvl=xlvl,
            startlat=startlat,
            startlon=startlon,
            deltalat=deltalat,
            deltalon=deltalon,
            dx=dx,
            dy=dy,
            xlonc=xlonc,
            truelat1=truelat1,
            truelat2=truelat2,
            earth_radius=earth_radius,
            slab=slab,
            is_wind_grid_rel=is_wind_grid_rel,
            startloc=startloc,
            field=field,
            hdate=hdate,
            units=units,
            map_source=map_source,
            desc=desc,
        )

