#!/bin/bash
# Usage: Project modis output onto a latlon proj to be read by WPS

# Uncomment for debugging purposes !
#set -x

if [ -z "$4" ]
then
    echo "!!!!! ERROR : Not enough arguments supplied !!!!!"
    echo "Please provide Start_dayofyear Start_year End_dayofyear End_year "
    echo "Exiting program"
    exit
fi

# First step : make virtual files gathering tiles of the same date
for y in $(seq ${2} ${4})
do
    for d in $(seq -f "%03g" ${1} ${3})
    do
	    echo "Processing ${d} ${y}"
        OUTFILE="./MODIS_${d}_${y}.vrt"
        gdalbuildvrt -sd 5 $OUTFILE "MOD10A1.A${y}${d}"*.hdf -overwrite
    done
done

# Second step: Loop over all .vrts to convert them as .tif AND project them in LatLon
# at the sale time.
for f in MODIS*.vrt
do
	echo "Processing file ${f}"
    OUTFILE=$(echo "$f"|sed 's/.vrt/.tif/')
    echo $OUTFILE
    
    gdalwarp -of GTIFF -s_srs '+proj=sinu +R=6371007.181 +nadgrids=@null +wktext' -r cubic -t_srs '+proj=longlat +datum=WGS84 +no_defs' $f  $OUTFILE -overwrite
done