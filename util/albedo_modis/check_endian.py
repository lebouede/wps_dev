import argparse
import struct

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="path to binary file to inspect")
    args = parser.parse_args()

    fin = open(args.file, "rb")
    print("Read as 4 bytes integers the file in Little endian")
    print(struct.unpack('<i', fin.read(4)))
    print(struct.unpack('<i', fin.read(4)))
    print(struct.unpack('<i', fin.read(4)))
    fin.close()

    print("Read as 4 bytes integers the file in Big endian")
    fin = open(args.file, "rb")
    print(struct.unpack('>i', fin.read(4)))
    print(struct.unpack('>i', fin.read(4)))
    print(struct.unpack('>i', fin.read(4)))
    fin.close()
